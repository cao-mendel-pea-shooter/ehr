// 权限拦截 导航守卫 路由守卫 router

import router from '@/router' // 引入路由实例
import store from '@/store' // 引入store实例 和组件中的this.$store是一回事

import nProgress from 'nprogress' // 导入进度条
import 'nprogress/nprogress.css' // 引入进度条的样式

// 不需要导出，因为只需要让代码执行即可
// 路由的前置守卫
// 后面跟一个函数 函数里有三个参数
// next是前置守卫必须执行的钩子 next必须执行，如果不执行，页面就死了
// next（）放过
// next（false）跳转终止
// next（地址）跳转到某个地址
const whiteList = ['/login', '/404'] // 定义一个白名单，里面放的是不需要token也可以访问的页面
router.beforeEach(async(to, from, next) => {
  nProgress.start() // 开启进度条的意思
  if (store.getters.token) {
    // 如果有token
    if (to.path === '/login') {
      // 就判断要访问的页面是不是 登录页
      // 如果是的就跳转到主页
      next('/')
    } else {
      // 不是就放行
      if (!store.getters.userId) {
        // 如果没有id这个值 才会调用 vuex的获取资料的action
        // 为什么要写await 因为我们想获取完资料再去放行
        await store.dispatch('user/getUserInfo')
      }
      next()
    }
  } else {
  // 如果没有token
    if (whiteList.indexOf(to.path) > -1) {
      // 判断要去的页面是否在白名单里面 大于-1表示在白名单里,直接放行
      next()
    } else {
    // 否则就直接跳转去登录页
      next('/login')
    }
  }
  nProgress.done() // 手动强制关闭一次 为了解决手动切换地址时，进度条不关闭的问题
})
// 路由的后置守卫
router.afterEach(() => {
  nProgress.done() // 关闭进度条的意思
})
