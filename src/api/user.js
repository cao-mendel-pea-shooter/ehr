import request from '@/utils/request'

// 登录请求模块
// 每个方法返回的都是个promise对象 所以可以用async 和 awite来获取以下的方法的结果
// data / params传参的不同是因为请求的方式不同 post用data，get用params
export function login(data) {
  return request({
    method: 'post',
    url: '/sys/login', // 因为所有的接口都要跨域 表示所有的接口要带/api
    data
  })
}

// 获取用户的基本资料
export function getUserInfo() {
  return request({
    method: 'post',
    url: '/sys/profile'
  })
}

/** *
 *
 * 获取用户的基本信息  现在写它 完全是为了显示头像
 * **/
export function getUserDetailById(id) {
  return request({
    url: `/sys/user/${id}`
  })
}

export function logout() {

}

