import request from '../utils/request'

/** *
 *
 * 获取组织架构数据
 * **/
export function getDepartments() {
  return request({
    url: '/company/department'
  })
}

/** *
 *  根据id根据部门  接口是根据restful的规则设计的   删除 delete  新增 post  修改put 获取 get
 * **/
export function delDepartments(id) {
  // request只是拦截
  return request({
    url: `/company/department/${id}`,
    method: 'delete'
    // method: 'DELETE'
  })
}

/**
 *  新增部门接口
 *
 * ****/
export function addDepartments(data) {
  return request({
    url: '/company/department/',
    method: 'post',
    data: data
  })
}

/** *
 * 获取部门详情
 * ***/
export function getDepartDetail(id) {
  return request({
    url: `/company/department/${id}`
  })
}

/**
 * 封装编辑部门接口
 *
 * ***/
// 文档中要id和修改后的body数据这两个参数
// 但是仔细思考会发现id是数据库传过来并且这里不能修改
// 干脆直接在data里面拿
// export function updateDepartments(data,id) {
export function updateDepartments(data) {
  return request({
    // url: `/company/department/${id}`,
    url: `/company/department/${data.id}`,
    method: 'put',
    data: data
  })
}
