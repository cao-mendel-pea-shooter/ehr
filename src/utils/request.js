// 导出一个axios实例，而且这个实例要有请求拦截器，响应拦截器

import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import { getTimeStamp } from '@/utils/auth'
import router from '@/router'
// 定义超时时间
const TimeOut = 3600
const service = axios.create({
  timeout: 5000, // 设置超时时间
  baseURL: process.env.VUE_APP_BASE_API // 基础地址
}) // 创建一个axios的实例

// 请求拦截器
service.interceptors.request.use(config => {
  if (store.getters.token) {
    // 只有在有token的情况下 才有必要去检查时间戳是否超时
    console.log('点击1')
    if (IsCheckTimeOut()) {
      console.log('点击2')
      // 如果它为true表示 过期了
      // token没用了 因为超时了
      // 登出操作
      store.dispatch('user/logout')
      // 跳转到登录页
      router.push('/login')
      return Promise.reject(new Error('token超时了'))
    }
    // 如果有token 就注入token
    // Authorization 是传递的参数，所以要['']包起来
    config.headers['Authorization'] = `Bearer ${store.getters.token}`
  }

  return config // 必须要返回配置
}, error => {
  // error 信息 里面 response的对象
  if (error.response && error.response.data && error.response.data.code === 10002) {
    // 当等于10002的时候 表示 后端告诉我token超时了
    // 登出action 删除token
    store.dispatch('user/logout')
    router.push('/login')
  } else {
    // 提示错误信息
    Message.error(error.message)
  }
  return Promise.reject(error)
})

// 响应拦截器
// 返回两个函数，第一个是成功的函数，第二个是失败的
service.interceptors.response.use(response => {
  //  axios默认加了一层data
  // 通过解构赋值的方法拿到服务器返回的数据（success：请求成功与否，message：请求的提示信息，data：服务器返回的真实数据）
  const { success, message, data } = response.data
  // 判断有没有成功
  if (success) {
  // 请求成功的情况下，返回data数据，之后无需解构
    return data
  } else {
    // 如果业务已经错误，就进catch
    // Message.error() 提示错误信息的固定写法，后面跟要提示的信息
    Message.error(message) // 提示错误信息
    // 因为我们这里没有可以用的错误对象，所以我们手动new一个错误对象然后把请求的提示信息放进去
    return Promise.reject(new Error(message))
  }
}, error => {
  // 如果失败
  Message.error(error.message) // 提示错误信息
  return Promise.reject(error) // 返回执行错误，让当前的执行链跳出成功，直接进入catch
})

// 是否超时
// 超时逻辑  (当前时间  - 缓存中的时间) 是否大于 时间差
function IsCheckTimeOut() {
  // 当前时间戳
  var currentTime = Date.now()
  // 缓存时间戳
  var timeStamp = getTimeStamp()
  return (currentTime - timeStamp) / 1000 > TimeOut
}

export default service // 导出axios实例
