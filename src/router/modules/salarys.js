// 导出属于员工的路由规则
import Layout from '@/layout'
// 每个子模块 其实 都是外层是layout  组件位于layout的二级路由里面
export default {
  // 路径
  path: '/salarys',
  // 给路由规则加一个name
  name: 'salarys',
  // 组件
  component: Layout,
  // 配置二级路的路由表
  children: [{
    // 这里当二级路由的path什么都不写的时候 表示该路由为当前二级路由的默认路由
    path: '',
    component: () => import('@/views/salarys'),
    // 路由元信息  其实就是存储数据的对象 我们可以在这里放置一些信息
    meta: {
      title: '工资管理',
      icon: 'money'
    }
  }]
}
