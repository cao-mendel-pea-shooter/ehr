// 引入缓存的cookie
import { getToken, setToken, removeToken, setTimeStamp } from '@/utils/auth'

// 引入登录的请求
import { login, getUserInfo, getUserDetailById } from '@/api/user'
// 状态
const state = {
  token: getToken(), // 设置一个token的共享状态 初始化的时候就从缓存中读取状态，并赋值到初始化的状态上
  // 定义一个空的对象 不是null 因为后边我要开放userInfo的属性给别人用  userInfo.name
  userInfo: {}
}
// 修改状态
const mutations = {
  // 设置token
  setToken(state, token) {
    state.token = token // 设置token，只能修改state的数据
    // 每次一有改动，就要同步给缓存
    setToken(token) // vuex和缓存数据的同步
  },
  // 删除token
  removeToken(state) {
    state.token = null // 清除vuex中的token
    removeToken() // 先清除vuex中的token，再清除缓存数据与vuex同步
  },
  // 设置用户信息
  setUserInfo(state, userInfo) {
    // / 用 浅拷贝的方式去赋值对象 因为这样数据更新之后，才会触发组件的更新
    state.userInfo = { ...userInfo }
  },
  // 删除用户信息
  removeUserInfo(state) {
    state.userInfo = {}
  }
}
// 执行异步
const actions = {
  // 这个login是actions的，自定义的名字
  async login(context, data) {
  // 这个login是封装的登录的请求方法
  // 得到一个promise对象 赋值给result
    const result = await login(data)
    // axios默认给数据添加了一层data
    // if (result.data.success) {
    // 如果登录成功
    // axios修改state必须通过mutations
    // 把成功的数据存入cookies缓存中
    // context.commit('setToken', result.data.data)

    // 因为我们的响应拦截器里已经解构了data，所以我们这里不需要再.data
    context.commit('setToken', result)
    // }
    // 写入时间戳
    setTimeStamp() // 将当前的最新时间写入缓存
  },
  async  getUserInfo(context) {
    // 获取用户资料action
    const result = await getUserInfo()
    // 将整个的个人信息设置到用户的vuex数据中
    // 为了获取头像
    const baseInfo = await getUserDetailById(result.userId)
    const baseResult = { ...result, ...baseInfo }
    // 将两个接口结果合并
    // 此时已经获取到了用户的基本资料 迫不得已 为了头像再次调用一个接口
    context.commit('setUserInfo', baseResult)

    // 加一个点睛之笔  这里这一步，暂时用不到，但是请注意，这给我们后边会留下伏笔
    return baseResult
  },
  // 登出的action
  logout(context) {
    // 删除token
    // 不仅仅删除了vuex中的 还删除了缓存中的
    context.commit('removeToken')
    // 删除用户资料
    context.commit('removeUserInfo')
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
